// @file    dataman.rs
// @author  Levi Perez (levianperez\@gmail.com)
// @date    2023-08-09

use log::*;


use chrono::prelude::*;
use chrono_tz::US::Pacific;


fn unix_now_sec() -> i64 {
    serenity::model::Timestamp::now().unix_timestamp()
}

fn timezone_offset_pt(now_sec: i64) -> i32 {

    match Pacific.timestamp_opt(now_sec, 0).latest() {

        None => -7 * 3600, // PST hardcoded fallback

        Some(now_pt) => {
            now_pt.offset().fix().local_minus_utc()
        }
    }
}


#[derive(Debug, Clone)]
pub struct DataPod {
    pub iid_hint:     i32,
    pub uid:          i64,
    // v2users.daily :
    pub last_seen:    i64,
    pub lt_xp:        i32,
    // v2users.weekly :
    pub xp_goal:      i32,
    pub goal_expires: i64,
    // v2users.rarely :
    pub xp_stride:    i32,
    pub goal_period:  i32,
    pub timezone:     i32,
    pub joined_at:    i64,
    // not in schema:
    pub synced:       bool
}

impl Default for DataPod {
    fn default() -> Self {
        let now_sec = unix_now_sec();

        let period = 60 * 60 * 24 * 7; // 7 days in seconds

        DataPod {
            iid_hint:     -1,
            uid:          0,
            last_seen:    now_sec,
            lt_xp:        0,
            xp_goal:      5,
            goal_expires: now_sec + period as i64,
            xp_stride:    6,
            goal_period:  period,
            // assume PT for now, can add /cmd to change per-user:
            timezone:     timezone_offset_pt(now_sec),
            joined_at:    now_sec,
            synced:       false
        }
    }
}


pub type DataMan = PostgresMan;


use sqlx::postgres::PgPoolOptions;
use crate::environment::Environment;
use serenity::model::id::UserId;

#[derive(Debug,Clone)]
pub struct PostgresMan {
    database: sqlx::Pool<sqlx::Postgres>,
    // TODO internal rate limiter, error counter
}

impl PostgresMan {

    #[must_use]
    pub async fn new(env : &Environment) -> Self {
        info!("Environment:\n{}", env.to_string());

        let options = PgPoolOptions::new()
            .max_connections(4) // TODO?
            ;

        let connection: Result<sqlx::Pool<sqlx::Postgres>, sqlx::Error>
            = options.connect(&env.psql_uri).await;

        match connection {
            Ok(pool) => PostgresMan {
                database: pool
            },
            Err(e) => panic!("PostgresMan error: {:?}", e)
        }
    }

    // trying both v1 (flat) and v2 (structured) schemas,
    // toggled via Cargo.toml feature flags~

    #[cfg(not(feature = "v2-schema"))]
    #[must_use]
    pub async fn load(&self, uid: UserId) -> DataPod {
        let raw_uid = uid.0 as i64;

        assert!(raw_uid != 0);

        // (queries must be string literals for the compile-time query linter to be happy)
        let fetched = sqlx::query!(
            "select * from v1users.all where uid = $1",
            raw_uid)
        .fetch_optional(&self.database)
        .await;

        match fetched.unwrap() {
            Some(record) => {
                DataPod {
                    iid_hint:     record.iid,
                    uid:          record.uid,
                    last_seen:    record.last_seen,
                    lt_xp:        record.lt_xp,
                    xp_goal:      record.xp_goal,
                    goal_expires: record.goal_expires,
                    xp_stride:    record.xp_stride,
                    goal_period:  record.goal_period,
                    timezone:     record.timezone,
                    joined_at:    record.joined_at,
                    synced:       true
                }
            },
            None => {
                let mut data = DataPod {
                    uid:    raw_uid,
                    synced: true,
                  ..Default::default()
                };

                self.store_all(&mut data).await;

                data
            }
        }
    }

    #[cfg(feature = "v2-schema")]
    #[must_use]
    pub async fn load(&self, uid: UserId) -> DataPod {
        unimplemented!()
    }


    /// Performs a full upsert, so should only be necessary to call when goal
    /// expiry is turning over for the individual user, or else a special
    /// command was called.
    #[cfg(not(feature = "v2-schema"))]
    pub async fn store_all(&self, data: &mut DataPod) {
        assert!(data.uid != 0);

        if data.iid_hint < 1 { // never been inserted
            let insert = sqlx::query!(
               "insert into v1users.all
                values(default,$1,$2,$3,$4,$5,$6,$7,$8,$9)
                on conflict on constraint all_uid_key do update
                  set (last_seen,lt_xp,xp_goal,goal_expires,xp_stride,goal_period,timezone,joined_at)
                    = ($2,$3,$4,$5,$6,$7,$8,$9)
                returning iid",
                data.uid,
                data.last_seen,
                data.lt_xp,
                data.xp_goal,
                data.goal_expires,
                data.xp_stride,
                data.goal_period,
                data.timezone,
                data.joined_at)
            .fetch_optional(&self.database)
            .await;

            if let Err(ahhhhh) = insert {
                error!("iid_hint={}: {:?}", data.iid_hint, ahhhhh);
                data.synced = false;
                // (fallthrough intentionally)
            }
            else if let Some(iid) = insert.unwrap().map(|record| record.iid) {
                info!("Found iid_hint: {}", iid);
                data.iid_hint = iid;
                data.synced = true;
                return;
            }
            else {
                panic!("unable to find an iid_hint...")
            }
        }

        // else, assume update existing row:
        let update = sqlx::query!(
           "update v1users.all set (
              last_seen,
              lt_xp,
              xp_goal,
              goal_expires,
              xp_stride,
              goal_period,
              timezone,
              joined_at) = ($2,$3,$4,$5,$6,$7,$8,$9)
            where uid = $1
            returning iid",
            data.uid,
            data.last_seen,
            data.lt_xp,
            data.xp_goal,
            data.goal_expires,
            data.xp_stride,
            data.goal_period,
            data.timezone,
            data.joined_at)
        .fetch_one(&self.database)
        .await;

        match update {
            Err(ahhhhh) => {
                error!("{:?}", ahhhhh);
                data.synced = false;
            },
            Ok(ret) => {
                if data.iid_hint != ret.iid {
                    assert!(data.iid_hint < 1);
                    data.iid_hint = ret.iid;
                }
                data.synced = true;
            }
        }
    }

    #[cfg(feature = "v2-schema")]
    pub async fn store_all(&self, data: &mut DataPod) {
        unimplemented!()
    }


    /// Cheaper store operation for the most common database event.
    /// Does not alter data.synced unless data has never been stored.
    #[cfg(not(feature = "v2-schema"))]
    pub async fn store_daily(&self, data: &mut DataPod) {
        if data.iid_hint < 1 {
            return self.store_all(data).await;
        }

        // daily: $3 last_seen i64,
        //        $4 lt_xp     i32

        let update = sqlx::query!(
           "update v1users.all set last_seen = $2, lt_xp = $3 where uid = $1",
            data.uid,
            data.last_seen,
            data.lt_xp)
        .execute(&self.database)
        .await;

        if let Err(ahhhhh) = update {
            error!("{:?}", ahhhhh);
        }
    }

    #[cfg(feature = "v2-schema")]
    pub async fn store_daily(&self, data: &mut DataPod) {
        unimplemented!()
    }


    #[cfg(not(feature = "v2-schema"))]
    pub async fn store_weekly(&self, data: &mut DataPod) {
        debug!("TODO: write weekly-specific optimizations");
        self.store_all(data).await
    }

    #[cfg(feature = "v2-schema")]
    pub async fn store_weekly(&self, data: &mut DataPod) {
        debug!("TODO: write weekly-specific optimizations");
        self.store_all(data).await
    }


    #[cfg(not(feature = "v2-schema"))]
    pub async fn add_xp(&self, data: &mut DataPod, amount: i32) {
        // (queries must be string literals for the compile-time query linter to be happy)
        let maybe = sqlx::query!(
            "update v1users.all set lt_xp = lt_xp + $1 where uid = $2 returning lt_xp",
            amount,
            data.uid) // TODO: bench vs iid_hint indexing
        .fetch_optional(&self.database)
        .await;

        if let Err(ahhhhh) = maybe {
            error!("{:?}", ahhhhh);
            data.lt_xp += amount;
            data.synced = false;
        }
        else if let Some(xp) = maybe.unwrap().map(|record| record.lt_xp) {
            info!("{} + {} = {}?", data.lt_xp, amount, xp);
            data.lt_xp = xp;
            data.synced &= true;
        }
        else {
            data.lt_xp += amount;
            data.synced = false;
        }
    }

    #[cfg(feature = "v2-schema")]
    pub async fn add_xp(&self, data: &mut DataPod, amount: i32) {
        unimplemented!()
    }
}


//============================== tests =======================================//

#[cfg(test)]
mod tests {

    use log::*;

    #[test]
    fn timezone_offset_pt() {
        let now = super::unix_now_sec();
        let tz  = super::timezone_offset_pt(now) / 3600;
        assert!(tz == -7 || tz == -8);
    }


    use crate::environment::Environment;
    use super::{DataPod,DataMan};
    use serenity::model::id::UserId;
    use std::sync::OnceLock;

    static XPBOY_ID: UserId = UserId(1135789554377433178u64);

    static TEST_MAN: OnceLock<DataMan> = OnceLock::new();


    async fn _get_test_man() -> &'static DataMan {
        let _ = env_logger::builder().is_test(true).try_init();
        match TEST_MAN.get() {
            Some(man) => man,
            None => {
                let _ = TEST_MAN.set(DataMan::new(&Environment::read()).await);
                TEST_MAN.get().unwrap()
            }
        }
    }


    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn load() {
        let test_man = _get_test_man().await;

        let data = test_man.load(XPBOY_ID).await;

        info!("XPBoy loaded: {:?}", data);

        assert_eq!(data.uid, XPBOY_ID.0 as i64);

        assert!(data.synced);
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn add_xp() {
        let test_man = _get_test_man().await;

        let mut data = test_man.load(XPBOY_ID).await;

        let mut last_xp = data.lt_xp;

        test_man.add_xp(&mut data, 1).await;

        assert!(data.lt_xp > last_xp);

        last_xp = data.lt_xp;

        test_man.add_xp(&mut data, 1).await;

        assert!(data.lt_xp > last_xp);
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn store_all() {
        let test_man = _get_test_man().await;

        let mut data = DataPod {
            uid:    XPBOY_ID.0 as i64,
            synced: false,
          ..Default::default()
        };

        let expect_ls = super::unix_now_sec();
        let expect_xg = data.xp_goal + 5;
        let expect_tz = 5 * 3600;

        data.last_seen = expect_ls;
        data.xp_goal   = expect_xg;
        data.timezone  = expect_tz;

        test_man.store_all(&mut data).await;

        assert!(data.synced);

        data = test_man.load(XPBOY_ID).await;

        assert_eq!(data.last_seen, expect_ls);
        assert_eq!(data.xp_goal,   expect_xg);
        assert_eq!(data.timezone,  expect_tz);
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn store_daily() {
        let test_man = _get_test_man().await;

        let mut data    = test_man.load(XPBOY_ID).await;
        let     last_xp = data.lt_xp;
        let     last_xg = data.xp_goal;

        data.lt_xp   += 2;
        data.xp_goal += 1337;

        test_man.store_daily(&mut data).await;

        data = test_man.load(XPBOY_ID).await;

        assert!(data.lt_xp > last_xp);
        assert_eq!(data.xp_goal, last_xg);
    }
}
