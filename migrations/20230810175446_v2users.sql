-- $ sqlx migrate run

create schema v2users;

create table v2users.daily (
-- as in "written to daily"
    uid          bigint   not null primary key,
    last_seen    bigint   not null, -- unix timestamp   (sec)
    lt_xp        integer  not null
);

create table v2users.weekly (
-- as in "written to weekly"
    uid          bigint   not null primary key,
    xp_goal      integer  not null,
    goal_expires bigint   not null  -- unix timestamp   (sec)
);

create table v2users.rarely (
-- as in "written to rarely"
    uid          bigint   not null primary key,
    xp_stride    integer  not null,
    goal_period  integer  not null, -- unix time offset (sec)
    timezone     integer  not null, -- unix time offset (sec)
    joined_at    bigint   not null  -- unix timestamp   (sec)
);

create table v2users.extras (
    uid          bigint   not null unique,
    raw_json     json         null  -- additional data
);


-- db-side procedures

create procedure v2users.mkuser(uid bigint, due bigint, goal integer, stride integer, period integer, timezone integer)
language sql
as $$
    insert into v2users.daily  values (
        uid,
        0,
        extract(epoch from now()::timestamptz(0))
    );
    insert into v2users.weekly values (
        uid,
        goal,
        due
    );
    insert into v2users.rarely values (
        uid,
        stride,
        period,
        timezone,
        extract(epoch from now()::timestamptz(0))
    );
$$;

create procedure v2users.rmuser(id bigint)
language sql
as $$
    delete from v2users.daily  where uid = id;
    delete from v2users.weekly where uid = id;
    delete from v2users.rarely where uid = id;
    delete from v2users.extras where uid = id;
$$;
