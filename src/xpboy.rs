// @file    xpboy.rs
// @author  Levi Perez (levianperez\@gmail.com)
// @date    2023-08-09

use crate::environment::Environment;
use crate::dataman::{DataMan,DataPod};

use log::*;

use serenity::async_trait;
use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::model::gateway::Ready;


pub struct XPBoy {
    datadaddy: DataMan
}

impl XPBoy {
    pub async fn new(env : &Environment) -> Self {
        Self {
            datadaddy: DataMan::new(env).await
        }
    }
}


#[async_trait]
impl EventHandler for XPBoy {

    async fn ready(&self, _ctx: Context, ready: Ready) {
        info!("{} connected!", ready.user.name);
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if msg.is_own(&ctx.cache) {
            return;
        }

        let _ = msg.channel_id.send_message(&ctx, |m| {
            m.content("noted!")
        })
        .await
        .unwrap();
    }
}
