// @file    main.rs
// @author  Levi Perez (levianperez\@gmail.com)
// @date    2023-08-09

mod environment;
mod dataman;
mod xpboy;

use crate::environment::Environment;
use crate::xpboy::*;

use log::*;

use serenity::prelude::*;


#[tokio::main]
async fn main() {

    let environment = Environment::read();
    info!("token: {:?}",    environment.token);
    info!("psql_uri: {:?}", environment.psql_uri);

    env_logger::init();

    let our_boy = XPBoy::new(&environment).await;

    let intents = GatewayIntents::GUILDS
                | GatewayIntents::GUILD_INTEGRATIONS
             // | GatewayIntents::GUILD_PRESENCES
                | GatewayIntents::GUILD_MESSAGES
                | GatewayIntents::GUILD_MESSAGE_REACTIONS
                | GatewayIntents::DIRECT_MESSAGES
                | GatewayIntents::DIRECT_MESSAGE_REACTIONS
                | GatewayIntents::MESSAGE_CONTENT // may be able to revoke?
                | GatewayIntents::GUILD_SCHEDULED_EVENTS // explore me
                ;

    let mut client = Client::builder(environment.token, intents)
            .event_handler(our_boy)
            .await
            .expect("Serenity Discord client failed to construct.");

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}
