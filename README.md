# **XP Boy** ~ a game-ified accountability bot for Discord

Welcome to the `deployed` branch!

This branch represents whatever the "XP Boy" bot is currently running live.


# Contributing

Refer to the README in the [**staging**](../../tree/staging) branch!

## Contributors:
- **Levi Perez** (aka @LyrePyre, 0xDEADFACE, levi@leviperez.dev)
- **Ayrton Muniz** (aka @AJ213, aj213)
