// @file    environment.rs
// @author  Levi Perez (levianperez\@gmail.com)
// @date    2023-08-09

use std::env;
use dotenv::dotenv;

pub struct Environment {
    pub token: String,
    pub psql_uri: String // turns out it's not necessary to read/store this manually~
}

impl Environment {
    pub fn read() -> Environment {
        let dotenv_loaded = false;
        let (token,    dotenv_loaded) = read_env("DISCORD_TOKEN", dotenv_loaded);
        let (psql_uri,       _      ) = read_env("PSQL_URI",      dotenv_loaded);
        Environment {
            token,
            psql_uri
        }
    }
}

impl ToString for Environment {
    fn to_string(&self) -> String {
        let mut bob = String::new();

        bob.push_str("DISCORD_TOKEN=");
        bob.push_str(&self.token);
        bob.push('\n');
        bob.push_str("PSQL_URI=");
        bob.push_str(&self.psql_uri);

        return bob;
    }
}

fn read_env(envkey:&str,retry:bool) -> (String,bool) {
    match env::var(envkey) {
        Ok(token) => (token,retry),
        Err(e)    => {
            if retry { panic!("no envvar \"{}\"! e: {:?}", envkey, e); }

            dotenv().ok();

            (read_env(envkey,true).0,true)
        }
    }
}
