-- $ sqlx migrate run

create schema v1users;

create table v1users.all (
    iid          serial   not null primary key,
    uid          bigint   not null unique,
    -- daily :
    last_seen    bigint   not null, -- unix timestamp   (sec)
    lt_xp        integer  not null,
    -- weekly :
    xp_goal      integer  not null,
    goal_expires bigint   not null, -- unix timestamp   (sec)
    -- rarely :
    xp_stride    integer  not null,
    goal_period  integer  not null, -- unix time offset (sec)
    timezone     integer  not null, -- unix time offset (sec)
    joined_at    bigint   not null  -- unix timestamp   (sec)
);

create table v1users.extras (
    uid          bigint   not null unique,
    raw_json     json         null  -- additional data
);

create table v1users.logs (
    at        timestamp  not null,
    raw_json  json           null
) inherits (v1users.all);
